
LIBS = -lm
OPTS = -std=c++11 -Wall 
DEBUGOPTS = -g -D DEBUG

lib:
	g++ *.cpp $(LIBS) $(OPTS) $(DEBUGOPTS) -c
	ar cr libobjpro.a *.o

test:
	g++ *.cpp $(LIBS) $(OPTS) $(DEBUGOPTS) -o test
