#include "objpro.h"

#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

namespace objpro {

void Model::push_vertex(float *values) {
    if(vpushed >= valloc)
        throw "Vertex buffer overflow";
    auto t = 4 * vpushed;
    float *buffer = vertices.get();
    for(int i = 0; i < 4; ++i)
        buffer[t++] = values[i];
    ++vpushed;
}

void Model::push_normal(float *values) {
    if(npushed >= nalloc)
        throw "Normal buffer overflow";
    auto t = 4 * npushed;
    float *buffer = normals.get();
    for(int i = 0; i < 4; ++i)
        buffer[t++] = values[i];
    ++npushed;
}

void Model::push_texcoord(float *values) {
    if(tcpushed >= tcalloc)
        throw "Texcoord buffer overflow";
    auto t = 4 * tcpushed;
    float *buffer = texcoords.get();
    for(int i = 0; i < 4; ++i)
        buffer[t++] = values[i];
    ++tcpushed;
}

void Model::push_face(Face face) {
    if(fpushed >= falloc)
        throw "Face buffer overflow";
    (faces.get())[fpushed++] = face;
}

Model::Model():
    valloc(4096), nalloc(4096), tcalloc(4096), falloc(4096),
    vpushed(0),   npushed(0),   tcpushed(0),   fpushed(0),
    vertices (new float[4 * valloc ]),
    normals  (new float[4 * nalloc ]),
    texcoords(new float[4 * tcalloc]),
    faces    (new Model::Face[falloc])
     {
    //vertices  = shared_ptr<float>(new float[4 * valloc ]);
    //normals   = shared_ptr<float>(new float[4 * nalloc ]);
    //texcoords = shared_ptr<float>(new float[4 * tcalloc]);
}

template<typename T>
T __try_to_read(stringstream &ss, T default_value) {
    T result;
    if(!ss.good()) return default_value;
    ss >> result;
    return (ss.good() || ss.eof()) ? result : default_value;
}

void __read_vec(string &data, float *target, float *default_values) {
    stringstream ss(data);
    for(int i = 0; i < 4; ++i)
        target[i] = __try_to_read<float>(ss, default_values[i]);
}

Model::Face __read_face(string &face_data) {
    Model::Face result;
    stringstream ss1(face_data);
    for(int t = 0; t < 3; ++t) {
        string vertex_data;
        getline(ss1, vertex_data, ' ');
        if(vertex_data == "")
            getline(ss1, vertex_data, ' ');
        stringstream ss2(vertex_data);
        string tmp;
        getline(ss2, tmp, '/');
        //cout << vertex_data << " | " << tmp << " || ";
        try        { result.vindices[t] = stoi(tmp) - 1;  }
        catch(...) { result.vindices[t] = 0;              }
        getline(ss2, tmp, '/');
        try        { result.tcindices[t] = stoi(tmp) - 1; }
        catch(...) { result.tcindices[t] = 0;             }
        getline(ss2, tmp);
        try        { result.nindices[t] = stoi(tmp) - 1;  }
        catch(...) { result.nindices[t] = 0;              }
    }
    //printf("Triangle: %d %d %d\n", result.vindices[0], result.vindices[1], result.vindices[2]);
    return result;
}

// MEMBER FUNCTION POINTERS, yeah!
typedef void (Model::*__vec4_setter)(float*);

void Loader::load(string filename) {
    ifstream f(filename);
    Model currModel;
    if(f.bad())
        throw "Couldn't open file for reading.";
    
    float default_vertex  [] = {0, 0, 0, 1};
    float default_normal  [] = {0, 0, 0, 0};
    float default_texcoord[] = {0, 0, 0, 0};
    map<string, __vec4_setter> setters;
    map<string, float*> defaults;
    setters["v" ] = &Model::push_vertex;    defaults["v" ] = default_vertex;
    setters["vn"] = &Model::push_normal;    defaults["vn"] = default_normal;
    setters["vt"] = &Model::push_texcoord;  defaults["vt"] = default_texcoord;
    string command, data;
    while(!(f >> command).eof()) {
        // comment
        if(command[0] == '#') {
            getline(f, data);
            continue;
        }
        // vertex, normal or texcoord
        if(command == "v" || command == "vn" || command == "vt") {
            float values[4];
            getline(f, data);
            __read_vec(data, values, defaults[command]);
            (currModel.*(setters[command]))(values);
            continue;
        }
        // face
        if(command == "f") {
            getline(f, data);
            auto face = __read_face(data);
            currModel.push_face(face);
            continue;
        }
        // unknown command
        cerr << "Unknown command: " << command << endl;
        getline(f, data);
    }
    models.push_front(currModel);
}

}// namespace objpro

/*int main() {
    objpro::Loader loader;
    loader.load("./CompDesk.obj");
}*/
