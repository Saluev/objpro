
#include <string>
#include <map>
#include <list>
#include <memory>

namespace objpro {

class Material {

};

class MaterialLibrary {
  public:
    std::map<std::string, Material> materials;
};

class Model {
  public:
    struct Face {
        unsigned short vindices[3], nindices[3], tcindices[3];
    };
  protected:
    size_t valloc, nalloc, tcalloc, falloc;
    size_t vpushed, npushed, tcpushed, fpushed;
    //void push_any     (float *what, Buffer? *where); // TODO
    void push_vertex  (float *);
    void push_normal  (float *);
    void push_texcoord(float *);
    void push_face(Face);
  public:
    size_t facesCount(void) { return fpushed; };
    std::shared_ptr<float> vertices, normals, texcoords;
    std::shared_ptr<Face> faces;
    Model();
    friend class Loader;
};


class Loader {
  public:
    std::list<Model> models;
    void load(std::string filename);
};

}//namespace objpro
